Display multiple hierarchical product catalog blocks
----------------------------------------------------

- Extract files under ubercart
- Enable the Multi Catalog Menu Module within the ubercart-core (optional)
- Goto admin/store/settings/catalog/edit/multiplecatalogblock
- State the total number of independent hierarchical product catalog block and Save Configuration
- After save configuration to goto block configuration page. You will see something like
  Catalog Block:1
  Catalog Block:2
- On block page Select configuration
- State the Catalog Term ID: (Note:just go over the catalog see the id Ex: catalog/25/xxxxx 25 is the ID)
- Save block.
- At last position your block in which Region you want to see.